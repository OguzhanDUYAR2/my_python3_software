#!/usr/bin/python3
#-*-coding:utf-8-*-

print('''**************
Vize Notu Hesaplama V1
Not: Program çan eğrisi olan sistemleri hesaplayamaz.
*************
''')
try:
    v1o = int(input("1.Vize notu yüzde kaç etkiliyor :"))
    v2o = int(input("2.Vize notu yüzde kaç etkiliyor ( Tek vize yapılıyorsa 0 girin ) :"))
    fo = int(input("Final Notu yüzde kaç etkiliyor :"))

    v1n = int(input("1.Vize notunuzu yazın :"))
    v2n = int(input("2.Vize notunuzu yazın ( Tek vize yapılıyorsa 0 girin ) :"))
    fn = int(input("Final notunuzu yazın :"))

    print("Üniversiteden üniversiteye harf notları değiştiği için lütfen harf notu değerlerini giriniz...")
    aa = int(input("Kaçtan fazla alırsanız AA notu alırsınız :"))
    ba = int(input("Kaçtan fazla alırsanız BA notu alırsınız :"))
    bb = int(input("Kaçtan fazla alırsanız BB notu alırsınız :"))
    cb = int(input("Kaçtan fazla alırsanız CB notu alırsınız :"))
    cc = int(input("Kaçtan fazla alırsanız CC notu alırsınız :"))
    dc = int(input("Kaçtan fazla alırsanız DC notu alırsınız :"))
    dd = int(input("Kaçtan fazla alırsanız DD notu alırsınız :"))
    fd = int(input("Kaçtan fazla alırsanız FD notu alırsınız :"))
    ff = int(input("Kaçtan az alırsanız FF notu alırsınız :"))

    gno = v2n * (v2o/100) + v1n * (v1o/100) + fn * (fo/100)

    if gno >= aa:
        print("Harf notunuz AA, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= ba:
        print("Harf notunuz BA, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= bb:
        print("Harf notunuz BB, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= cb:
        print("Harf notunuz CB, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= cc:
        print("Harf notunuz CC, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= dc:
        print("Harf notunuz DC, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= dd:
        print("Harf notunuz DD, genel not ortalamanız {:.2f}.".format(gno))

    elif gno >= fd:
        print("Harf notunuz FD, genel not ortalamanız {:.2f}.".format(gno))

    elif gno < ff:
        print("Harf notunuz FF ve dersden kaldınız, genel not ortalamanız {:.2f}.".format(gno))

    else:
        print("Hesaplanamadı tekrar deneyiniz.....")

except ValueError:
    print("Lütfen tam sayı giriniz.....")

